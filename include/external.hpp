#pragma once
// external libraries
#include <vulkan/vulkan.hpp>
//
#include <GLFW/glfw3.h>
//

//
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <optional>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
